<?php

use MaoNaRoda\Backend\Controller\ContatoController;

return [
    '/contato' => ContatoController::class,
    '/cadastrar' => UsuarioController::class
];
