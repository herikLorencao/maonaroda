<?php

namespace MaoNaRoda\Backend\Model\Dao;

use MaoNaRoda\Backend\Model\Domain\Contato;

class ContatoDAO
{
    private $conexao = null;

    public function __construct($conexao)
    {
        $this->conexao = $conexao;
    }

    public function inserir(Contato $contato): bool
    {

        $sql = "INSERT INTO contato(nome, email, texto) VALUES (?, ?, ?)";

        $stmt = $this->conexao->prepare($sql);

        return $stmt->execute([
            $contato->getNome(),
            $contato->getEmail(),
            $contato->getTexto()
        ]);
    }
}
