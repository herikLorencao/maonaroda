<?php

namespace MaoNaRoda\Backend\Model\Database;

use PDO;
use PDOException;

class Conexao
{
    public static function getConnection()
    {
        $servidor = '127.0.0.1';
        $banco = 'mao_na_roda';
        $usuario = 'postgres';
        $senha = 'postgres';

        try {
            $conexao = new PDO("pgsql:host=$servidor;dbname=$banco", $usuario, $senha);
        } catch (PDOException $e) {
            die('Erro: ' . $e->getMessage());
        }

        return $conexao;
    }
}
