<?php

namespace MaoNaRoda\Backend\Model\Domain;

class Contato
{
    private $nome;
    private $email;
    private $texto;

    public function __construct($nome = null, $email = null, $texto = null)
    {
        $this->nome = $nome;
        $this->email = $email;
        $this->texto = $texto;
    }

    public function getNome(): string
    {
        return $this->nome;
    }

    public function setNome(string $nome)
    {
        $this->nome = $nome;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function getTexto(): string
    {
        return $this->texto;
    }

    public function setTexto(string $texto)
    {
        $this->texto = $texto;
    }
}
