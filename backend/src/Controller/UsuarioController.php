<?php

namespace MaoNaRoda\Backend\Controller\UsuarioController;

use MaoNaRoda\Backend\Model\Database\Conexao;
use Nyholm\Psr7\Response;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use MaoNaRoda\Backend\Model\Domain\Usuario;

class UsuarioController implements RequestHandlerInterface
{
    public function handle($request): ResponseInterface
    {
        $array_dados = json_decode($request->getBody(), true);

        $conexao = Conexao::getConnection();

        $usuario = new Usuario();

        $usuario->setNome(filter_var($array_dados['nome'], FILTER_SANITIZE_STRING));
        $usuario->setTelefone(filter_var($array_dados['telefone'], FILTER_SANITIZE_STRING));
        $usuario->setEmail(filter_var($array_dados['email'], FILTER_SANITIZE_STRING));
        $usuario->setSenha(filter_var($array_dados['senha'], FILTER_SANITIZE_STRING));


        return new Response(200, [], 'suco de maracuja');
    }
}
