<?php

namespace MaoNaRoda\Backend\Controller;

use MaoNaRoda\Backend\Model\Dao\ContatoDAO;
use MaoNaRoda\Backend\Model\Database\Conexao;
use MaoNaRoda\Backend\Model\Domain\Contato;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ServerRequestInterface;


class ContatoController implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $array_dados = json_decode($request->getBody(), true);

        $conexao = Conexao::getConnection();
        $contato = new Contato($array_dados['nome'], $array_dados['email'], $array_dados['texto']);

        $contatoDAO = new ContatoDAO($conexao);

        $resultado = $contatoDAO->inserir($contato);

        if ($resultado) {
            $msg = 'Formulário enviado com sucesso!';
        } else {
            $msg = 'Houve problemas no envio desse formulário!';
        }

        return new Response(200, ['Content-Type' => 'application/json'], json_encode($msg));
    }
}
