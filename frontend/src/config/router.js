import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from "../components/pages/Home"
import Contato from "../components/pages/Contato"
import Login from '../components/pages/Login'
import Cadastro from '../components/pages/Cadastro'
import EsqueceuSenha from '../components/pages/EsqueceuSenha'
import Conta from '../components/pages/Conta'
import RelatarProblema from '../components/pages/RelatarProblema'

Vue.use(VueRouter)

const routes = [{
    name: 'home',
    path: '/',
    component: Home
},
{
    name: 'contato',
    path: '/contato',
    component: Contato
},
{
    name: 'login',
    path: '/login',
    component: Login
},
{
    name: 'cadastrar',
    path: '/cadastrar',
    component: Cadastro
},
{
    name: 'esqueceusenha',
    path: '/esqueceusenha',
    component: EsqueceuSenha
},
{
    name: 'conta',
    path: '/conta',
    component: Conta
},
{
    name: 'problema',
    path: '/problema',
    component: RelatarProblema
}
]

const router = new VueRouter({
    mode: 'history',
    routes
})



export default router