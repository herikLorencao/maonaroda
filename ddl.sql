CREATE TABLE contato (
  id INTEGER NOT NULL SERIAL,
  nome VARCHAR(50) NOT NULL,
  email VARCHAR(50) NOT NULL,
  texto VARCHAR(50) NOT NULL,
  CONSTRAINT pk_contato PRIMARY KEY (id)
);
CREATE TABLE usuario(
  id INTEGER NOT NULL SERIAL,
  nome VARCHAR (50) NOT NULL,
  email VARCHAR (50) NOT NULL,
  senha VARCHAR (50) NOT NULL,
  CONSTRAINT pk_usuario PRIMARY KEY (id)
);
CREATE TABLE tipo_problema (
  id INTEGER NOT NULL SERIAL,
  nome VARCHAR(45) NOT NULL,
  CONSTRAINT pk_tipo_problema PRIMARY KEY (id)
);
CREATE TABLE formulario_problema(
  id INTEGER NOT NULL SERIAL,
  usuario INTEGER NOT NULL,
  tipo INTEGER NOT NULL,
  texto VARCHAR(255) NOT NULL,
  CONSTRAINT pk_formulario_problema PRIMARY KEY (id),
  CONSTRAINT fk_formulario_problema_usuario FOREIGN KEY (usuario) REFERENCES usuario(id),
  CONSTRAINT fk_formulario_problema_tipo FOREIGN KEY (tipo) REFERENCES tipo_problema(id)
);
